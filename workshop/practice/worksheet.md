Skript für den Git-Workshop zum Durcharbeiten
=============================================

(Dauer mit 9 Teilnehmern: ca. 4 Stunden)

## Teil 0: Benutzerkonfiguration anlegen

```
// Aktuellen Stand abfragen
$ git config --help
$ git config -l
> user.email=moini@noreply.invalid
> user.name=Moini
> user.signingkey=ABCDEFGHI
> push.default=simple
> core.repositoryformatversion=0
> core.filemode=true
> core.bare=false
> core.logallrefupdates=true

// Name und Email für alle Repos festlegen (ohne --global: nur für aktuelles Repo)
$ git config --global user.email <email>
$ git config --global user.name <name>

// Stand nochmal abfragen
$ git config -l

> ...
```

## Teil 1: Lokales Repository anlegen und erste Commits machen

### Repository anlegen:

```
$ cd /tmp
$ mkdir mein_echo
$ cd mein_echo/
$ ls -A
$ git init .
$ ls -A
```

### Dateien 'mein_echo.sh' und 'README' erstellen

```
$ nano mein_echo.sh
$ nano README.md
```

### Datei 'mein_echo.sh' ausführbar machen und ausführen

```
$ chmod +x mein_echo.sh
$ ./mein_echo.sh

> Polly ist ein Papagei!
> ('ende' beendet)
$ Hallo!
> Polly sagt:
> Hallo!
> Polly ist ein Papagei!
> ('ende' beendet)
$ ende
> Polly sagt:
> ende
```

### Aktuellen Stand überprüfen und erster Commit

```
// Status abfragen
$ git status

> Auf Branch master
> 
> Noch keine Commits
> 
> Unversionierte Dateien:
>  (benutzen Sie "git add <Datei>...", um die Änderungen zum Commit vorzumerken)
>
>        README.md
>        mein_echo.sh
>
> nichts zum Commit vorgemerkt, aber es gibt unversionierte Dateien
> (benutzen Sie "git add" zum Versionieren)

> $ git add README.md
> $ git status
> Auf Branch master
> 
> Noch keine Commits
> 
> zum Commit vorgemerkte Änderungen:
>   (benutzen Sie "git rm --cached <Datei>..." zum Entfernen aus der Staging-Area)
> 
>         neue Datei:     README.md
> 
> Unversionierte Dateien:
>   (benutzen Sie "git add <Datei>...", um die Änderungen zum Commit vorzumerken)
> 
>         mein_echo.sh

// Datei zum Commit vormerken
$ git add mein_echo.sh
$ git status

> Auf Branch master
> 
> Noch keine Commits
> 
> zum Commit vorgemerkte Änderungen:
>   (benutzen Sie "git rm --cached <Datei>..." zum Entfernen aus der Staging-Area)
> 
>         neue Datei:     README.md
>         neue Datei:     mein_echo.sh

// Datei aus der staging area entfernen (Änderungen bleiben erhalten)
$ git reset README.md
$ git status

> Auf Branch master
> 
> Noch keine Commits
> 
> zum Commit vorgemerkte Änderungen:
>   (benutzen Sie "git rm --cached <Datei>..." zum Entfernen aus der Staging-Area)
> 
>         neue Datei:     mein_echo.sh
> 
> Unversionierte Dateien:
>   (benutzen Sie "git add <Datei>...", um die Änderungen zum Commit vorzumerken)
> 
>         README.md

// Alle Dateien aus der staging area entfernen (Änderungen bleiben erhalten)
$ git reset
$ git status

> Auf Branch master
> 
> Noch keine Commits

> Unversionierte Dateien:
>   (benutzen Sie "git add <Datei>...", um die Änderungen zum Commit vorzumerken)
> 
>         README.md
>         mein_echo.sh
> 
> nichts zum Commit vorgemerkt, aber es gibt unversionierte Dateien
> (benutzen Sie "git add" zum Versionieren)

// Alle Dateien zum Commit vormerken
$ git add *
$ git status

> Auf Branch master
> 
> Noch keine Commits
> 
> zum Commit vorgemerkte Änderungen:
>   (benutzen Sie "git rm --cached <Datei>..." zum Entfernen aus der Staging-Area)
> 
>         neue Datei:     README.md
>         neue Datei:     mein_echo.sh

// Commithistorie abfragen
$ git log

> fatal: Ihr aktueller Branch 'master' hat noch keine Commits.

// Erster Commit

$ git commit -m "Erste Version des Echo-Programms. Gibt jetzt die Eingabe wieder aus."

> [master (Basis-Commit) f6d9c57] Erste Version des Echo-Programms. Gibt jetzt die Eingabe wieder aus.
>  2 files changed, 18 insertions(+)
>  create mode 100644 README.md
>  create mode 100755 mein_echo.sh

// Nochmal Historie abfragen

$ git log

> commit f6d9c57222641a6ba7b3ec3bece3d44fc83fb09e (HEAD -> master)
> Author: Moini <moini@noreply.invalid>
> Date:   Mon Aug 26 23:43:19 2019 +0200
> 
>     Erste Version des Echo-Programms. Gibt jetzt die Eingabe wieder aus.
```

### Noch ein lokaler Commit (alle committen, Unterschiede anzeigen)

```

// Datei 'LICENSE' anlegen.
$ nano LICENSE

$ git status

> Auf Branch master
> Unversionierte Dateien:
>   (benutzen Sie "git add <Datei>...", um die Änderungen zum Commit vorzumerken)
> 
>         LICENSE.md
> 
> nichts zum Commit vorgemerkt, aber es gibt unversionierte Dateien
> (benutzen Sie "git add" zum Versionieren)

// Unterschiede anzeigen
$ git diff

> ... // Änderungen werden angezeigt

// Alle neuen Dateien committen
$ git commit -a -m "Lizenz hinzugefügt."

> [master 5a795dc] Lizenz hinzugefügt.
>  1 file changed, 4 insertions(+)
>  create mode 100644 LICENSE.md
```

## Teil 2: Entscheiden, welche Dateien von git beachtet werden

### Dateien ignorieren

```
// Geheime Passwortdatei 'passwords.sh' anlegen. 
$ nano passwords.sh
$ git status

> ... // Datei erscheint in den Änderungen

// Außerdem eine Passwortmusterdatei 'passwords.template.sh' anlegen.

$ nano passwords.template.sh
$ git status

> ... // Beide Dateien erscheinen in den Änderungen

// '.gitignore'-Datei anlegen.

$ nano .gitignore
$ git status

> ... // passwords.sh ist nicht mehr aufgeführt.

$ git add *

> ... // git meldet, dass die Passwortdatei nicht mit dabei ist.

// Achtung: die .gitignore-Datei wird aber auch nicht gestaged! (ist nicht in * der bash mit drin) Muss separat hinzugefügt werden.

$ git add .gitignore

$ git status

> Auf Branch master
> zum Commit vorgemerkte Änderungen:
>  (benutzen Sie "git reset HEAD <Datei>..." zum Entfernen aus der Staging-Area)
>
>        neue Datei:     .gitignore
>        neue Datei:     passwords.template.sh

$ git commit -m "Passwortmusterdatei angelegt."

> [master cb58bc2] Passwortmusterdatei angelegt.
> 2 files changed, 4 insertions(+)
> create mode 100644 .gitignore
> create mode 100644 passwords.template.sh
```

### Nochmal den aktuellen Status und Verlauf ansehen

```
$ git status

> Auf Branch master
> nichts zu committen, Arbeitsverzeichnis unverändert

$ git log

> commit cb58bc217ec5d114eeacd49c409455398824b32a (HEAD -> master)
> Author: Moini <moini@noreply.invalid>
> Date:   Mon Aug 26 23:51:55 2019 +0200
> 
>     Passwortmusterdatei angelegt.
> 
> commit 5a795dc5abf588a0ea2bf9b088ed8d15314aca6d
> Author: Moini <moini@noreply.invalid>
> Date:   Mon Aug 26 23:48:23 2019 +0200
> 
>     Lizenz hinzugefügt.
> 
> commit f6d9c57222641a6ba7b3ec3bece3d44fc83fb09e
> Author: Moini <moini@noreply.invalid>
> Date:   Mon Aug 26 23:43:19 2019 +0200
> 
>     Erste Version des Echo-Programms. Gibt jetzt die Eingabe wieder aus.
```

### Leeres Verzeichnis anlegen und committen

Git trackt nur Dateien, keine Verzeichnisse! Daher muss eine leere Datei in einem Verzeichnis angelegt werden, wenn man es schon mal 'haben' möchte, und erst später zu trackende Dateien darin anlegen will. 

```
$ mkdir user_uploads
$ git status

> Auf Branch master
> nichts zu committen, Arbeitsverzeichnis unverändert

// .gitkeep-Datei anlegen (kann auch jeden anderen Namen haben)
$ nano user_uploads/.gitkeep
$ git status

> Auf Branch master
> Unversionierte Dateien:
>   (benutzen Sie "git add <Datei>...", um die Änderungen zum Commit vorzumerken)
> 
>         user_uploads/
> 
> nichts zum Commit vorgemerkt, aber es gibt unversionierte Dateien
> (benutzen Sie "git add" zum Versionieren)


$ git add *

> Die folgenden Pfade werden durch eine Ihrer ".gitignore" Dateien ignoriert:
> passwords.sh
> Verwenden Sie -f wenn Sie diese wirklich hinzufügen möchten.

$ git status

> ... // .gitkeep wird jetzt getrackt

$ git diff

> ... // nichts!!!

$ git diff --staged

> ... // alles in der staging area

$ git commit -m "Verzeichnis für Benutzeruploads hinzugefügt."

> [master 5f14e82] Verzeichnis für Benutzeruploads hinzugefügt.
>  1 file changed, 0 insertions(+), 0 deletions(-)
>  create mode 100644 user_uploads/.gitkeep

$ git status
> Auf Branch master
> nichts zu committen, Arbeitsverzeichnis unverändert
```

Hinweis: wenn man den Ordner haben will, aber die Dateien später nicht tracken möchte, legt man einfach eine .gitignore-Datei mit dem Inhalt '*' darin an. Diese sollte man dann aber tracken ;-)

## Teil 4: Verzweigung und Merge

### Lokaler Zweig für neues Feature

Wer ist Polly? Wir wollen Tux!

```
// Zweig erzeugen
$ git branch tux
$ git status

> Auf Branch master
> nichts zu committen, Arbeitsverzeichnis unverändert

// Zu Zweig wechseln
$ git checkout tux

> Zu Branch 'tux' gewechselt

$ git status

> Auf Branch tux
> nichts zu committen, Arbeitsverzeichnis unverändert

// Datei mein_echo.sh entsprechend anpassen
$ nano mein_echo.sh // Nicht komplett anpassen!
$ git status

> Auf Branch tux
> Änderungen, die nicht zum Commit vorgemerkt sind:
>   (benutzen Sie "git add <Datei>...", um die Änderungen zum Commit vorzumerken)
>   (benutzen Sie "git checkout -- <Datei>...", um die Änderungen im Arbeitsverzeichnis zu verwerfen)
> 
>         geändert:       mein_echo.sh
> 
> keine Änderungen zum Commit vorgemerkt (benutzen Sie "git add" und/oder "git commit -a")


$ git diff

> diff --git a/mein_echo.sh b/mein_echo.sh
> index 21f3aad..b4d514e 100755
> --- a/mein_echo.sh
> +++ b/mein_echo.sh
> @@ -2,7 +2,7 @@
> 
>  var="start"
> 
> -echo "Polly ist ein Papagei!
> +echo "Tux ist ein Pinguin!
>  ('ende' beendet)"
> 
>  while [ "$var" != "ende" ]

$ git status

> Auf Branch tux
> Änderungen, die nicht zum Commit vorgemerkt sind:
>   (benutzen Sie "git add <Datei>...", um die Änderungen zum Commit vorzumerken)
>   (benutzen Sie "git checkout -- <Datei>...", um die Änderungen im Arbeitsverzeichnis zu verwerfen)
> 
>         geändert:       mein_echo.sh
> 
> keine Änderungen zum Commit vorgemerkt (benutzen Sie "git add" und/oder "git commit -a")

$ git commit -a -m "Tux-Feature eingebaut."

> [tux 3eeccf2] Tux-Feature eingebaut.
>  1 file changed, 1 insertion(+), 1 deletion(-)

$ git status

> Auf Branch tux
>  nichts zu committen, Arbeitsverzeichnis unverändert


$ git log

> ... // langes Log
```

### Oh nein, ein Fehler im letzten Commit!!!

```
$ ./mein_echo.sh

> Tux ist ein Pinguin!
> ('ende' beendet)
> Du sagst:
> Hallo!
> Polly sagt:
> Hallo!
> Du sagst:
> ende
> Polly sagt:
> ende

// Mist. Da ist noch Polly drin!!!

// Commit rückgängig machen.
$ git reset HEAD~

> Nicht zum Commit vorgemerkte Änderungen nach Zurücksetzung:
> M       mein_echo.sh

$ git status

> Auf Branch tux
> Änderungen, die nicht zum Commit vorgemerkt sind:
>   (benutzen Sie "git add <Datei>...", um die Änderungen zum Commit vorzumerken)
>   (benutzen Sie "git checkout -- <Datei>...", um die Änderungen im Arbeitsverzeichnis zu verwerfen)
> 
>         geändert:       mein_echo.sh
> 
> keine Änderungen zum Commit vorgemerkt (benutzen Sie "git add" und/oder "git commit -a")

// Datei korrigieren, diff angucken, neu committen.

$ nano mein_echo.sh
$ git status
$ git diff
$ git commit -a -m "Tux-Feature eingebaut."

> [tux 78ef548] Tux-Feature eingebaut.
>  1 file changed, 2 insertions(+), 2 deletions(-)
```

### Änderungen übernehmen

```
// Noch sind wir auf dem Zweig 'tux'
$ git status

> Auf Branch tux
> nichts zu committen, Arbeitsverzeichnis unverändert

// zu 'master' wechseln
$ git checkout master

> Zu Branch 'master' gewechselt

// Dateien haben sich geänder! Der Papagei ist wieder da.
$ ./mein_echo.sh
> Polly ist ein Papagei!
> ('ende' beendet)
> Du sagst:
> Hallo Polly!
> Polly sagt:
> Hallo Polly!
> Du sagst:
> ende
> Polly sagt:
> ende

// Jetzt Tux übernehmen
$ git merge // Tab zum Gucken

> HEAD        master      ORIG_HEAD   tux

$ git merge tux

> Aktualisiere 5f14e82..78ef548
> Fast-forward
>  mein_echo.sh | 4 ++--
>  1 file changed, 2 insertions(+), 2 deletions(-)

// Wer ist jetzt da?

$ ./mein_echo.sh

> ... // Tux ist da.
```

### Merge-Konflikte

```
// Änderungen in beiden Zweigen an derselben Datei in derselben Zeile vornehmen
// Einer mag Konqui, ein anderer Tuxina.

$ git status

> Auf Branch master
> ...

$ git checkout -b konqui

> Zu neuem Branch 'konqui' gewechselt

$ git status

> Auf Branch konqui

$ nano mein_echo.sh
$ git status
$ git commit -a -m "Jetzt spricht Konqui. Außerdem den Kommentar aktualisiert."

// währenddessen im master-Zweig ...
$ git checkout master
$ nano mein_echo.sh
$ git commit -a -m "Unsere Heldin heißt jetzt Tuxina"

// Hallo, ich hab da tolle Änderungen im Kommentar gemacht, bitte übernehmen!
$ git merge konqui

> automatischer Merge von mein_echo.sh
> KONFLIKT (Inhalt): Merge-Konflikt in mein_echo.sh
> Automatischer Merge fehlgeschlagen; beheben Sie die Konflikte und committen Sie dann das Ergebnis.

$ git status

Auf Branch master
> Sie haben nicht zusammengeführte Pfade.
>   (beheben Sie die Konflikte und führen Sie "git commit" aus)
>   (benutzen Sie "git merge --abort", um den Merge abzubrechen)
>
> Nicht zusammengeführte Pfade:
>   (benutzen Sie "git add/rm <Datei>...", um die Auflösung zu markieren)
>
>         von beiden geändert:    mein_echo.sh
>
> keine Änderungen zum Commit vorgemerkt (benutzen Sie "git add" und/oder "git commit -a")

// Jetzt suchen wir die Problemstellen und entscheiden, wie es am Ende sein soll.
$ nano mein_echo.sh
$ git add mein_echo.sh
$ git status

> Auf Branch master
> Alle Konflikte sind behoben, aber Sie sind immer noch beim Merge.
>   (benutzen Sie "git commit", um den Merge abzuschließen)
> 
> zum Commit vorgemerkte Änderungen:
> 
>         geändert:       mein_echo.sh

$ git commit -m "Tuxina hat jetzt mehr Feuer."
[master 28caa3a] Tux hat jetzt mehr Feuer.

$ git log

> ... // Commithistorie mit gemergeten Commits

// Hinweis: viele tolle Optionen:
$ git log --help
```

## Teil 5: Umgang mit entfernten Repositories

### 1. Eigenes Repo ins Netz bringen

* eigenen git-Server anlegen
* GitLab / GitHub / Framagit etc. nutzen

#### GitLab als git-Server nutzen

* Registrieren / Einloggen
* + -> New Project
* Name ausfüllen
* Sichtbarkeit setzen (für unseren Test bitte auf public)

#### Unserem lokalen Repo mitteilen, dass es das entfernte gibt

```
$ git remote add origin git@gitlab.com:Moini/wstest.git
$ git status

> Auf Branch master
> nichts zu committen, Arbeitsverzeichnis unverändert

$ git push
// Benutzername, Passwort eingeben

// Nur wenn es den entsprechenden Zweig noch nicht im entfernten Repo gibt:
> fatal: Der aktuelle Branch master hat keinen Upstream-Branch.
> Um den aktuellen Branch zu versenden und den Remote-Branch
> als Upstream-Branch zu setzen, benutzen Sie
>
>    git push --set-upstream origin master

// entsprechenden Zweig im entfernten Repo anlegen
$ git push --set-upstream origin master

> Zähle Objekte: 28, Fertig.
> Delta compression using up to 4 threads.
> Komprimiere Objekte: 100% (24/24), Fertig.
> Schreibe Objekte: 100% (28/28), 3.16 KiB | 808.00 KiB/s, Fertig.
> Total 28 (delta 11), reused 0 (delta 0)
> To gitlab.com:Moini/wstest.git
>  * [new branch]      master -> master
> Branch 'master' folgt nun Remote-Branch 'master' von 'origin'.

// Jetzt weiter committen, pushen, committen, pushen, mergen ...
```

### 2. Mitarbeit an fremden Repos (ohne Commitrechte)

* Ohne Commitrechte muss man auf andere Weise um Aufnahme der eigenen Änderungen in das fremde Repository bitten.
* Diese sind eine Funktion des Webinterface / werden abhängig von der Projektkultur geregelt.
  - GitLab: Merge request (MR)
  - Github: Pull request (PR)
  - Kernelmailingliste: Patchdatei an Email anhängen
* Daher für GitLab: 
  - eigene Kopie auf deren Webseite nötig ("Fork")
  - daran kann man online über Webinterface arbeiten oder
  - man kann einen Clone anlegen und lokal arbeiten (sinnvoller, wenn man den Code auch testen will)

#### Öffentlichen SSH-Schlüssel auf GitLab hinterlegen für Clonen über SSH (optional)

* SSH-Schlüssel generieren: `ssh-keygen -t rsa -b 4096 -C "you@computer-name"`
* Auf GitLab: Benutzerbild oben rechts -> Settings -> SSH keys
* Öffentlichen Schlüssel ('id_rsa.pub') hineinkopieren und benennen
* Spart die wiederholte Eingabe des Passwortes
* Schwierig mit Passwort geschützten keys (glaube ich)

#### Einen eigenen Fork anlegen

* Projektseite besuchen
* Auf Fork klicken

#### Repository clonen (eine lokale Kopie anlegen)

* Auf Forkseite gehen in GitLab
* Clone -> Eintrag kopieren (wenn SSH-Schlüssel bei GitLab hinterlegt, den SSH-Link nehmen, sonst den anderen)

```
$ cd projects

// das Repo in das aktuelle Verzeichnis clonen (mit Punkt: die Dateien werden im aktuellen Verzeichnis abgelegt; ohne Punkt: ein Verzeichnis für das Repo wird angelegt; Pfad/Ordnername statt Punkt: der Ordner wird ggf. angelegt)
$ git clone [git@gitlab.com:Moini/gitWS.git | https://gitlab.com/Moini/gitWS.git]

$ cd <reponame>
$ git checkout -b "feature_branch"

$ <am Repo arbeiten, testen, kompilieren, ...>
$ git status
$ git diff
$ git add ...
$ git commit -a -S -m "Dieses tolle neue Feature hinzugefügt."
$ git push --set-upstream origin feature_branch
// Da Capo Al Fine (ohne --set-upstream)
```
* Wieder auf Forkseite gehen
* GitLab bietet einem bereits an, einen MR zu machen
* Genau gucken, von wo nach wo!
* Formular ausfüllen, erklären was es tut
* Abwarten

### 3. Eigenen Code aktualisieren

Immer, wenn man anfängt, an etwas neuem zu arbeiten, wenn man neue Änderungen ausprobieren möchte, oder wenn man seine Änderungen in einem Merge request aufnehmen lassen möchte, sollte man dafür sorgen, dass der eigene Code auf dem aktuellsten Stand ist.

#### Einfach: Keine lokalen Änderungen

```
$ git pull
```

#### Kompliziert: Lokale und entfernte Änderungen - Rebase

Manchmal kann es passieren, dass sehr viel Zeit vergeht, bevor ein MR angeschaut werden kann, und sich das fremde Repo weiterentwickelt hat.
Dasselbe Problem kann man auch in einem Repo haben, bei dem man Commitrechte hat (dem eigenen 'Upstream'), an dem aber auch andere mitarbeiten.

Dann ist ein direkter Merge oft nicht mehr möglich (über das Webinterface).
Wenn man den Code lokal mittels 'git pull [ggf. von irgendwoher]' aktualisiert, erzeugt das merge commits ('Update to master'), die später die Commithistorie mit belanglosen Infos füllen.

Man sollte dann möglichst einen sog. Rebase machen, jedoch nur, wenn nicht bereits jemand anderes seinerseits an den Änderungen mitgearbeitet hat - also nur, wenn er ganz uns allein gehört. Ein Rebase ordnet die Geschichte neu, und basiert die eigenen Änderungen auf dem aktuellen Stand des fremden Repos.

```
$ git checkout feature_branch
$ git remote --add upstream git@gitlab.com:Moini/gitWS.git
$ git pull --rebase upstream master
// jetzt die Merge-Konflikte auflösen, es geht auch `git fetch` und `rebase --interactive`.
```
